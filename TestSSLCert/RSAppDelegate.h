//
//  RSAppDelegate.h
//  TestSSLCert
//
//  Created by Pablo Santa Cruz on 2/19/14.
//  Copyright (c) 2014 Pablo Santa Cruz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
