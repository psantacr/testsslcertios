//
//  main.m
//  TestSSLCert
//
//  Created by Pablo Santa Cruz on 2/19/14.
//  Copyright (c) 2014 Pablo Santa Cruz. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RSAppDelegate class]));
    }
}
