//
//  RSViewController.h
//  TestSSLCert
//
//  Created by Pablo Santa Cruz on 2/19/14.
//  Copyright (c) 2014 Pablo Santa Cruz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/NSURLConnection.h>
#import <Foundation/NSURLAuthenticationChallenge.h>

@interface RSViewController : UIViewController <NSURLConnectionDelegate>

@end
